from pathlib import Path
import shutil

from jinja2 import Environment, FileSystemLoader


env = Environment(
  loader = FileSystemLoader("templates"),
  trim_blocks = True,
  lstrip_blocks = True,
)


Path("./templates/HEPI/End/Target/").mkdir(parents=False, exist_ok=True)

with open('./templates/HEPI/End/Target/IO.TcGVL', mode="w", encoding="utf-8") as f:
  f.write(env.get_template("HEPI/End/Source/IO.template").render())

with open('./templates/HEPI/End/Target/MAIN.TcPOU', mode="w", encoding="utf-8") as f:
  f.write(env.get_template("HEPI/End/Source/MAIN.template").render())

with open('./templates/HEPI/End/Target/GAUGE.TcPOU', mode="w", encoding="utf-8") as f:
  f.write(env.get_template("HEPI/Source/GAUGE.template").render())

with open('./templates/HEPI/End/Target/LATCH.TcPOU', mode="w", encoding="utf-8") as f:
  f.write(env.get_template("HEPI/Source/LATCH.template").render())

with open('./templates/HEPI/End/Target/PUMP_CTRL.TcPOU', mode="w", encoding="utf-8") as f:
  f.write(env.get_template("HEPI/Source/PUMP_CTRL.template").render())

shutil.copyfile('./templates/IO/FUN/EL3164_COUNTS_TO_VOLTS.TcPOU', './templates/HEPI/End/Target/EL3164_COUNTS_TO_VOLTS.TcPOU')
shutil.copyfile('./templates/IO/FUN/EL3602_0010_COUNTS_TO_MILLI_VOLTS.TcPOU', './templates/HEPI/End/Target/EL3602_0010_COUNTS_TO_MILLI_VOLTS.TcPOU')
shutil.copyfile('./templates/IO/FUN/EL4104_VOLTS_TO_COUNTS.TcPOU', './templates/HEPI/End/Target/EL4104_VOLTS_TO_COUNTS.TcPOU')
shutil.copyfile('./templates/IO/FUN/SCALE_LINEAR.TcPOU', './templates/HEPI/End/Target/SCALE_LINEAR.TcPOU')
