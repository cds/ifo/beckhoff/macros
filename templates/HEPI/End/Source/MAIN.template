{% import 'OPC.template' as OPC %}
<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject>
  <POU Name="MAIN" SpecialFunc="None">
    <Declaration><![CDATA[PROGRAM MAIN
VAR
  PS_PRESS1: GAUGE; (*~
(OPC :1: Make variable visible)*)

  PS_PRESS2: GAUGE; (*~
(OPC :1: Make variable visible)*)

  PS_PRESS3: GAUGE; (*~
(OPC :1: Make variable visible)*)

  PS_PRESS4: GAUGE; (*~
(OPC :1: Make variable visible)*)

  BSCSUP_PRESS: GAUGE; (*~
(OPC :1: Make variable visible)*)

  BSCRET_PRESS: GAUGE; (*~
(OPC :1: Make variable visible)*)

  PRESS_DIFF_PSI: LREAL; (*~
{{OPC.analog(access='1', egu='psi', prec='2')}}*)

  {attribute 'TcRetain'}
  PRESS_DIFF_PSI_LOW: LREAL; (*~
{{OPC.analog(access='3', egu='psi', prec='2')}}*)

  {attribute 'TcRetain'}
  PRESS_DIFF_PSI_HIGH: LREAL; (*~
{{OPC.analog(access='3', egu='psi', prec='2')}}*)

  PRESS_DIFF_PSI_OOR: BOOL; (*~
{{OPC.binary(access='1', onam='Out Of Range', znam='Not Out Of Range', osv='MAJOR', zsv='NO_ALARM')}}*)


  PRESS_HIGH: BOOL; (*~
{{OPC.binary(access='1', onam='Error', znam='No Error', osv='MAJOR', zsv='NO_ALARM')}}*)


  PRESS_OK: BOOL; (*~
{{OPC.binary(access='1', onam='Ok', znam='Not Ok', osv='NO_ALARM', zsv='MAJOR')}}*)


  LEVEL_SENSOR: BOOL; (*~
{{OPC.binary(access='1', onam='Enable', znam='Disable', osv='NO_ALARM', zsv='MAJOR')}}*)


  VFD_READ_VOLTS: LREAL; (*~
{{OPC.analog(access='1', egu='V', prec='2')}}*)

  VFD_READ_HZ: LREAL; (*~
{{OPC.analog(access='1', egu='Hz', prec='2')}}*)


  CTRL_LOC: PUMP_CTRL; (*~
(OPC :1: Make variable visible)*)

  CTRL_RMT: PUMP_CTRL; (*~
(OPC :1: Make variable visible)*)


  CTRL_SELECT: BOOL; (*~
{{OPC.binary(access='3', onam='DIFF', znam='PS_PRESS4')}}*)

  CTRL_OUTPUT: LREAL; (*~
{{OPC.analog(access='1', egu='V', prec='2')}}*)


  VFD_INTLK: LATCH; (*~
(OPC :1: Make variable visible)*)


  VFD_CTRL_VOLTS: LREAL; (*~
{{OPC.analog(access='1', egu='V', prec='2')}}*)
END_VAR
VAR CONSTANT
  PRESS_HIGH_LIMIT: LREAL := 115; (*~
{{OPC.analog(access='1')}}*)
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[PS_PRESS1(MV := EL3602_0010_COUNTS_TO_MILLI_VOLTS(IO.MR_TERM_2_CHAN1_IN_COUNTS));

PS_PRESS2(MV := EL3602_0010_COUNTS_TO_MILLI_VOLTS(IO.MR_TERM_2_CHAN2_IN_COUNTS));

PS_PRESS3(MV := EL3602_0010_COUNTS_TO_MILLI_VOLTS(IO.MR_TERM_3_CHAN1_IN_COUNTS));

PS_PRESS4(MV := EL3602_0010_COUNTS_TO_MILLI_VOLTS(IO.MR_TERM_3_CHAN2_IN_COUNTS));

BSCSUP_PRESS(MV := EL3602_0010_COUNTS_TO_MILLI_VOLTS(IO.VEA_TERM_9_CHAN1_IN_COUNTS));

BSCRET_PRESS(MV := EL3602_0010_COUNTS_TO_MILLI_VOLTS(IO.VEA_TERM_9_CHAN2_IN_COUNTS));

PRESS_DIFF_PSI := BSCSUP_PRESS.PSI - BSCRET_PRESS.PSI;
PRESS_DIFF_PSI_OOR := PRESS_DIFF_PSI < PRESS_DIFF_PSI_LOW OR PRESS_DIFF_PSI > PRESS_DIFF_PSI_HIGH;

PRESS_HIGH := PS_PRESS1.PSI > PRESS_HIGH_LIMIT;

PRESS_OK := NOT (PS_PRESS4.PSI_OOR OR PRESS_DIFF_PSI_OOR);


LEVEL_SENSOR := IO.MR_TERM_6_CHAN1_IN_BOOL;


VFD_READ_VOLTS := EL3164_COUNTS_TO_VOLTS(IO.MR_TERM_4_CHAN1_IN_COUNTS);

(* 0.0 V = 0.0 Hz, 10.0 V = 60.0 Hz *)
VFD_READ_HZ := SCALE_LINEAR(0.0, 10.0, 0.0, 60.0, VFD_READ_VOLTS);


CTRL_LOC(
  INPUT := PS_PRESS4.PSI
);

CTRL_RMT(
  INPUT := PRESS_DIFF_PSI
);


IF CTRL_SELECT = FALSE THEN
  CTRL_OUTPUT := CTRL_LOC.OUTPUT;
ELSE
  CTRL_OUTPUT := CTRL_RMT.OUTPUT;
END_IF


VFD_INTLK(
  INPUT := NOT PRESS_HIGH
);

IF VFD_INTLK.OUTPUT = TRUE THEN
  VFD_CTRL_VOLTS := CTRL_OUTPUT;
ELSE
  VFD_CTRL_VOLTS := 0.0;
END_IF


IO.MR_TERM_5_CHAN1_OUT_COUNTS := EL4104_VOLTS_TO_COUNTS(VFD_CTRL_VOLTS);
]]></ST>
    </Implementation>
  </POU>
</TcPlcObject>
