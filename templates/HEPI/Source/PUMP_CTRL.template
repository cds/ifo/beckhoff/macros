{% import 'OPC.template' as OPC %}
<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject>
  <POU Name="PUMP_CTRL" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK PUMP_CTRL
VAR_INPUT
  INPUT: LREAL; (*~
{{OPC.analog(access='1', egu='psi', prec='2')}}*)
END_VAR
VAR_OUTPUT
  OUTPUT: LREAL; (*~
{{OPC.analog(access='1', egu='V', prec='2')}}*)
END_VAR
VAR
  {attribute 'TcRetain'}
  SETPT: LREAL; (*~
{{OPC.analog(access='3', egu='psi', prec='2')}}*)

  {attribute 'TcRetain'}
  SETPT_HIGH: LREAL; (*~
{{OPC.analog(access='3', egu='psi', prec='2')}}*)

  {attribute 'TcRetain'}
  SETPT_LOW: LREAL; (*~
{{OPC.analog(access='3', egu='psi', prec='2')}}*)

	SETPT_OOR: BOOL; (*~
{{OPC.binary(access='1', onam='Out Of Range', znam='Not Out Of Range', osv='MAJOR')}}*)

  {attribute 'TcRetain'}
  SETPT_MAX_LIM: LREAL; (*~
{{OPC.analog(access='3', egu='psi', prec='2')}}*)

  {attribute 'TcRetain'}
  SETPT_MIN_LIM: LREAL; (*~
{{OPC.analog(access='3', egu='psi', prec='2')}}*)

  MODE: BOOL; (*~
{{OPC.binary(access='3', onam='Auto', znam='Manual')}}*)

  {attribute 'TcRetain'}
  TIME_PER_UPDATE: UDINT; (*~
{{OPC.analog(access='3', egu='x100ns')}}*)

  ITERM_RESET_REQ: BOOL; (*~
{{OPC.binary(access='3')}}*)

  {attribute 'TcRetain'}
  OUTPUT_TWEAK_SIZE: LREAL; (*~
{{OPC.analog(access='3', prec='2')}}*)

  OUTPUT_TWEAK_REQ: INT; (*~
{{OPC.analog(access='3', desc='0 = Reset, 1 = Positive, 2 = Negative')}}*)

  {attribute 'TcRetain'}
  KP: LREAL; (*~
{{OPC.analog(access='3', desc='Proportional Gain', prec='6')}}*)

  {attribute 'TcRetain'}
  KI: LREAL; (*~
{{OPC.analog(access='3', desc='Integral Gain', prec='6')}}*)

  {attribute 'TcRetain'}
  OFFSET: LREAL; (*~
{{OPC.analog(access='3', prec='2')}}*)

  {attribute 'TcRetain'}
  ITERM_MAX_LIM: LREAL; (*~
{{OPC.analog(access='3', prec='2')}}*)

  {attribute 'TcRetain'}
  ITERM_MIN_LIM: LREAL; (*~
{{OPC.analog(access='3', prec='2')}}*)

  {attribute 'TcRetain'}
  OUTPUT_MAX_LIM: LREAL; (*~
{{OPC.analog(access='3', egu='V', prec='2')}}*)

  {attribute 'TcRetain'}
  OUTPUT_MIN_LIM: LREAL; (*~
{{OPC.analog(access='3', egu='V', prec='2')}}*)

  ITERM_RESET_REQ_LATCH: R_TRIG; (*~
(OPC :0: Make variable invisible)*)

  OUTPUT_TWEAK_REQ_LATCH: R_TRIG; (*~
(OPC :0: Make variable invisible)*)

  ERROR: LREAL; (*~
{{OPC.analog(access='1', prec='2')}}*)

  KTERM: LREAL; (*~
{{OPC.analog(access='1', prec='2')}}*)

  ITERM: LREAL; (*~
{{OPC.analog(access='1', prec='2')}}*)

  fbGetCurTaskIdx: GETCURTASKINDEX; (*~
(OPC :0: Make variable invisible)*)

  TIME_PER_CYCLE: UDINT;

  CYCLES_PER_UPDATE: UDINT;

  CYCLE_COUNTER: UDINT := 0;
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[TIME_PER_CYCLE := _TaskInfo[fbGetCurTaskIdx.index].CycleTime;
CYCLES_PER_UPDATE := TIME_PER_UPDATE / TIME_PER_CYCLE;


SETPT_OOR := SETPT > SETPT_HIGH OR SETPT < SETPT_LOW;

IF SETPT > SETPT_MAX_LIM THEN
	SETPT := SETPT_MAX_LIM;
ELSIF SETPT < SETPT_MIN_LIM THEN
	SETPT := SETPT_MIN_LIM;
END_IF


ITERM_RESET_REQ_LATCH(CLK := ITERM_RESET_REQ);
IF ITERM_RESET_REQ_LATCH.Q = TRUE THEN
  ITERM := 0;
END_IF


OUTPUT_TWEAK_REQ_LATCH(CLK := (OUTPUT_TWEAK_REQ <> 0));


IF MODE = FALSE THEN
  (* Manual *)

  IF OUTPUT_TWEAK_REQ_LATCH.Q = TRUE THEN
    IF OUTPUT_TWEAK_REQ = 1 THEN
      OUTPUT := OUTPUT + OUTPUT_TWEAK_SIZE;
    ELSIF OUTPUT_TWEAK_REQ = 2 THEN
      OUTPUT := OUTPUT - OUTPUT_TWEAK_SIZE;
    END_IF
  END_IF

  CYCLE_COUNTER := 0;
ELSE
  (* Automatic *)

  CYCLE_COUNTER := CYCLE_COUNTER + 1;

  IF CYCLE_COUNTER >= CYCLES_PER_UPDATE THEN
    ERROR := SETPT - INPUT;

    (* Proportional *)
    KTERM := KP * ERROR;

    (* Integral *)
    ITERM := ITERM + KI * ERROR;
    IF ITERM > ITERM_MAX_LIM THEN
      ITERM := ITERM_MAX_LIM;
    ELSIF ITERM < ITERM_MIN_LIM THEN
      ITERM := ITERM_MIN_LIM;
    END_IF

    (* Output *)
    OUTPUT := KTERM + ITERM + OFFSET;
		IF OUTPUT > OUTPUT_MAX_LIM THEN
			OUTPUT := OUTPUT_MAX_LIM;
		ELSIF OUTPUT < OUTPUT_MIN_LIM THEN
			OUTPUT := OUTPUT_MIN_LIM;
		END_IF

    CYCLE_COUNTER := 0;
  END_IF
END_IF
]]></ST>
    </Implementation>
  </POU>
</TcPlcObject>
