{% import 'OPC.template' as OPC %}
{% import 'XML.template' as XML %}


{% macro declaration(name_prefix, link_prefix) %}
(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
(* EL4104 4 Channel Analog Output 0 - 10 V 16 bit *)
{% for i in range(1, 5) %}


(* Channel {{i}} *)

{attribute 'TcLinkTo' := '{{link_prefix}}^AO Output Channel {{i}}^Analog output'}
{{name_prefix}}CHAN{{i}}_OUT_COUNTS AT %Q*: INT; (*~
{{OPC.analog(access='1', egu='Counts')}}*)

{{name_prefix}}CHAN{{i}}_OUT_COUNTS_ERROR: BOOL; (*~
{{OPC.binary(access='1', onam='Error', znam='No Error', osv='MAJOR', zsv='NO_ALARM')}}*)

{{name_prefix}}CHAN{{i}}_OUT_VOLTS: LREAL; (*~
{{OPC.analog(access='1', egu='V')}}*)

{{name_prefix}}CHAN{{i}}_OUT_VOLTS_ERROR: BOOL; (*~
{{OPC.binary(access='1', onam='Error', znam='No Error', osv='MAJOR', zsv='NO_ALARM')}}*)
{% endfor %}


(* WcState *)

{attribute 'TcLinkTo' := '{{link_prefix}}^WcState^WcState'}
{{name_prefix}}WCSTATE_WCSTATE AT %I*: BOOL; (*~
{{OPC.binary(access='1', onam='Data invalid', znam='Data valid', osv='MAJOR')}}*)


(* InfoData *)

{attribute 'TcLinkTo' := '{{link_prefix}}^InfoData^State'}
{{name_prefix}}INFODATA_STATE AT %I*: UINT; (*~
{{OPC.analog(access='1')}}*)
{% endmacro %}


{% macro implementation(name_prefix) %}
(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
(* EL4104 *)
{% for i in range(1, 5) %}


(* Channel {{i}} *)

{{name_prefix}}CHAN{{i}}_OUT_COUNTS_ERROR := {{name_prefix}}WCSTATE_WCSTATE = TRUE OR {{name_prefix}}INFODATA_STATE <> 8;

{{name_prefix}}CHAN{{i}}_OUT_VOLTS_ERROR := {{name_prefix}}CHAN{{i}}_OUT_COUNTS_ERROR;

{{name_prefix}}CHAN{{i}}_OUT_COUNTS := EL4104VoltsToCounts({{name_prefix}}CHAN{{i}}_OUT_VOLTS);
{% endfor %}
{% endmacro %}


{% macro volts_to_counts_declaration() %}
FUNCTION EL4104VoltsToCounts : INT
VAR_INPUT
  VOLTS: LREAL;
END_VAR
VAR CONSTANT
  (* 0.0 V = 0 counts, 10.0 V = 32767 counts *)

  X1: LREAL := 0.0;
  X2: LREAL := 10.0;

  Y1: INT := 0;
  Y2: INT := 32767;
END_VAR
{% endmacro %}

{% macro volts_to_counts_implementation() %}
IF (VOLTS < 0.0) THEN
  VOLTS := 0.0;
ELSIF (VOLTS > 10.0) THEN
  VOLTS := 10.0;
END_IF;

EL4104VoltsToCountsFun := LREAL_TO_INT(LinearConversionFun(X1, X2, INT_TO_LREAL(Y1), INT_TO_LREAL(Y2), VOLTS));
{% endmacro %}

{% macro volts_to_counts() %}
{{XML.POU(name='EL4104VoltsToCounts', declaration=volts_to_counts_declaration(), implementation=volts_to_counts_implementation())}}
{% endmacro %}
