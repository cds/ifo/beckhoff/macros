{% import 'OPC.template' as OPC %}


FUNCTION_BLOCK EL3602_0010
  (* EL3602-0010 2 Channel Analog Input +/- 75 mV Diff 24 bit *)

VAR
{% for i in range(1, 3) %}
  (* Channel {{i}} *)

  CHAN{{i}}_UNDERRANGE AT %I*: BOOL; (*~
{{OPC.binary(access='1', onam='Underrange', znam='Not Underrange', osv='MAJOR')}}*)

  CHAN{{i}}_OVERRANGE AT %I*: BOOL; (*~
{{OPC.binary(access='1', onam='Overrange', znam='Not Overrange', osv='MAJOR')}}*)

  CHAN{{i}}_LIMIT1 AT %I*: UINT; (*~
{{OPC.binary(access='1')}}*)

  CHAN{{i}}_LIMIT2 AT %I*: UINT; (*~
{{OPC.binary(access='1')}}*)

  CHAN{{i}}_ERROR AT %I*: BOOL; (*~
{{OPC.binary(access='1', onam='Out of range', znam='In range', osv='MAJOR')}}*)

  CHAN{{i}}_TXPDOSTATE AT %I*: BOOL; (*~
{{OPC.binary(access='1', onam='TxPDO is not valid', znam='TxPDO is valid', osv='MAJOR')}}*)

  CHAN{{i}}_TXPDOTOGGLE AT %I*: BOOL; (*~
{{OPC.binary(access='1')}}*)

  CHAN{{i}}_IN_COUNTS AT %I*: DINT; (*~
{{OPC.analog(access='1', egu='Counts')}}*)

  CHAN{{i}}_IN_COUNTS_ERROR: BOOL; (*~
{{OPC.binary(access='1', onam='Error', znam='No Error', osv='MAJOR', zsv='NO_ALARM')}}*)
{% if not loop.last %}


{% endif %}
{% endfor %}


  (* WcState *)

  WCSTATE_WCSTATE AT %I*: BOOL; (*~
{{OPC.binary(access='1', onam='Data invalid', znam='Data valid', osv='MAJOR')}}*)

  WCSTATE_INPUTTOGGLE AT %I*: BOOL; (*~
{{OPC.binary(access='1')}}*)


  (* InfoData *)

  INFODATA_STATE AT %I*: UINT; (*~
{{OPC.analog(access='1')}}*)
END_VAR
VAR_OUTPUT
{% for i in range(1, 3) %}
  (* Channel {{i}} *)

  CHAN{{i}}_IN_MV: LREAL; (*~
{{OPC.analog(access='1', egu='mV')}}*)

  CHAN{{i}}_IN_MV_ERROR: BOOL; (*~
{{OPC.binary(access='1', onam='Error', znam='No Error', osv='MAJOR', zsv='NO_ALARM')}}*)
{% if not loop.last %}


{% endif %}
{% endfor %}
END_VAR
VAR CONSTANT
  (* -8388607 counts = -75.0 mV, 8388607 counts = 75.0 mV *)

  X1: DINT := -8388607;
  X2: DINT := 8388607;

  Y1: LREAL := -75.0;
  Y2: LREAL := 75.0;
END_VAR

-----

{% for i in range(1, 3) %}
(* Channel {{i}} *)

CHAN{{i}}_IN_COUNTS_ERROR := WCSTATE_WCSTATE = TRUE OR INFODATA_STATE <> 8 OR CHAN{{i}}_ERROR = TRUE OR CHAN{{i}}_TXPDOSTATE = TRUE;

CHAN{{i}}_IN_MV_ERROR := CHAN{{i}}_IN_COUNTS_ERROR;

CHAN{{i}}_IN_MV := LINEAR_CONVERSION(DINT_TO_LREAL(X1), DINT_TO_LREAL(X2), Y1, Y2, DINT_TO_LREAL(CHAN{{i}}_IN_COUNTS));
{% if not loop.last %}


{% endif %}
{% endfor %}
